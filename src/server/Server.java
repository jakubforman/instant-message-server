package server;

import java.io.*;
import java.net.*;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.*;


class ActiveHandlers {
    //private static final long serialVersionUID = 1L;
    private ConcurrentHashMap<String, SocketHandler> activeHandlersSet = new ConcurrentHashMap<>();

    /**
     * sendMessageToAll - Pošle zprávu všem aktivním klientům kromě sebe sama
     *
     * @param sender  - reference odesílatele
     * @param message - řetězec se zprávou
     */
    synchronized void sendMessageToAll(server.SocketHandler sender, String message) {
        for (SocketHandler handler : activeHandlersSet.values()) {// pro všechny aktivní handlery
            if (handler != sender) {
                if (!handler.messages.offer(message))   // zkus přidat zprávu do fronty jeho zpráv
                    System.err.printf("Client %s message queue is full, dropping the message!\n", handler.clientID);
            }
        }
    }

    /**
     * Zaslání zprávy uživateli
     *
     * @param sender     Odesílatel
     * @param receiverID Příjemce
     * @param message    Zpráva
     */
    synchronized void sendMessageTo(SocketHandler sender, String receiverID, String message) {
        if (!activeHandlersSet.containsKey(receiverID)) {
            System.err.printf("Client %s not found! Sender: %s\n", receiverID, sender.clientID);
            return;
        }

        SocketHandler handler = activeHandlersSet.get(receiverID);
        if (!handler.messages.offer(message))   // zkus přidat zprávu do fronty jeho zpráv
            System.err.printf("Client %s message queue is full, dropping the message!\n", handler.clientID);

    }

    /**
     * add přidá do množiny aktivních handlerů nový handler.
     * Metoda je sychronizovaná, protože HashSet neumí multithreading.
     *
     * @param handler - reference na handler, který se má přidat.
     * @return true if the set did not already contain the specified element.
     */
    synchronized void add(SocketHandler handler) {
        //boolean returnValue = false;
        if (!activeHandlersSet.containsKey(handler.clientID))
          //  returnValue = true;

        System.out.println("Adding " + handler.clientID);
        activeHandlersSet.put(handler.clientID, handler);

    }

    // část premiového úkolu
    synchronized void rename(String oldName, String newName) {
        if (!activeHandlersSet.containsKey(oldName))
            return;

        activeHandlersSet.put(newName, activeHandlersSet.remove(oldName));
    }

    /**
     * remove odebere z množiny aktivních handlerů nový handler.
     * Metoda je sychronizovaná, protože HashSet neumí multithreading.
     *
     * @param handler - reference na handler, který se má odstranit
     * @return true if the set did not already contain the specified element.
     */
    synchronized void remove(SocketHandler handler) {
        if (!activeHandlersSet.containsKey(handler.clientID))
            return;

        activeHandlersSet.remove(handler.clientID);
    }
}


class SocketHandler {
    /**
     * mySocket je socket, o který se bude tento SocketHandler starat
     */
    private Socket mySocket;

    /**
     * client ID je řetězec ve formátu <IP_adresa>:<port>
     */
    String clientID;

    /**
     * activeHandlers je reference na množinu všech právě běžících SocketHandlerů.
     * Potřebujeme si ji udržovat, abychom mohli zprávu od tohoto klienta
     * poslat všem ostatním!
     */
    private final server.ActiveHandlers activeHandlers;

    /**
     * messages je fronta příchozích zpráv, kterou musí mít kažý klient svoji
     * vlastní  - pokud bude je přetížená nebo nefunkční klientova síť,
     * čekají zprávy na doručení právě ve frontě messages
     */
    ArrayBlockingQueue<String> messages = new ArrayBlockingQueue<>(20);

    /**
     * startSignal je synchronizační závora, která zařizuje, aby oba tasky
     * OutputHandler.run() a InputHandler.run() začaly ve stejný okamžik.
     */
    private CountDownLatch startSignal = new CountDownLatch(2);

    /**
     * outputHandler.run() se bude starat o OutputStream mého socketu
     */
    server.SocketHandler.OutputHandler outputHandler = new server.SocketHandler.OutputHandler();
    /**
     * inputHandler.run()  se bude starat o InputStream mého socketu
     */
    server.SocketHandler.InputHandler inputHandler = new server.SocketHandler.InputHandler();
    /**
     * protože v outputHandleru nedovedu detekovat uzavření socketu, pomůže mi inputFinished
     */
    private volatile boolean inputFinished = false;

    SocketHandler(Socket mySocket, server.ActiveHandlers activeHandlers) {
        this.mySocket = mySocket;
        clientID = mySocket.getInetAddress().toString() + ":" + mySocket.getPort();
        this.activeHandlers = activeHandlers;
    }

    class OutputHandler implements Runnable {
        public void run() {
            OutputStreamWriter writer;
            try {
                System.err.println("DBG>Output handler starting for " + clientID);
                startSignal.countDown();
                startSignal.await();
                System.err.println("DBG>Output handler running for " + clientID);
                writer = new OutputStreamWriter(mySocket.getOutputStream(), StandardCharsets.UTF_8);
                writer.write("\nYou are connected from " + clientID + "\n");
                writer.flush();
                while (!inputFinished) {
                    String m = messages.take();// blokující čtení - pokud není ve frontě zpráv nic, uspi se!
                    writer.write(m + "\r\n");    // pokud nějaké zprávy od ostatních máme,
                    writer.flush();             // pošleme je našemu klientovi
                    System.err.println("DBG>Message sent to " + clientID + ":" + m + "\n");
                }
            } catch (IOException | InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            System.err.println("DBG>Output handler for " + clientID + " has finished.");

        }
    }

    class InputHandler implements Runnable {
        public void run() {
            try {
                System.err.println("DBG>Input handler starting for " + clientID);
                startSignal.countDown();
                startSignal.await();
                System.err.println("DBG>Input handler running for " + clientID);
                String request;
                /* v okamžiku, kdy nás Thread pool spustí, přidáme se do množiny
                 *  všech aktivních handlerů, aby chodily zprávy od ostatních i nám
                 */
                activeHandlers.add(SocketHandler.this);
                BufferedReader reader = new BufferedReader(new InputStreamReader(mySocket.getInputStream(), StandardCharsets.UTF_8));
                while ((request = reader.readLine()) != null) {

                    // přišla od mého klienta nějaká zpráva?
                    // ano - pošli ji všem ostatním klientům
                    // Pokud zadám -- tak spouštím příkaz na server
                    if (request.contains("-- ")) {
                        // potřeba změny na pole abych zjistil co vlastně přichází za příkazy
                        String[] splitted = request.replace("-- ", "").split(" ");
                        if (splitted.length != 2) {
                            // pošlu zprávu sám sobě
                            activeHandlers.sendMessageTo(SocketHandler.this,
                                    SocketHandler.this.clientID,
                                    "Spatny format soukrome zpravy");
                        } else {
                            if (splitted[0].equals("SetName")) {
                                System.out.println("Nastavuji jméno z " + clientID + " na " + splitted[1]);
                                activeHandlers.rename(clientID, splitted[1]);
                                clientID = splitted[1];
                            } else {
                                request = "Od " + clientID + " pro " + splitted[0] + ": " + splitted[1];
                                System.out.println(request);
                                String private_msg = "Soukromá zpráva od " + SocketHandler.this.clientID + ": " + splitted[1];
                                activeHandlers.sendMessageTo(SocketHandler.this, splitted[0], private_msg);
                            }
                        }
                    } else {
                        request = "Od " + clientID + ": " + request;
                        System.out.println(request);
                        activeHandlers.sendMessageToAll(SocketHandler.this, request);
                    }
                }
                inputFinished = true;
                messages.offer("OutputHandler, wakeup and die!");
            } catch (InterruptedException | IOException e) {
                e.printStackTrace();
            } finally {
                // remove yourself from the set of activeHandlers
                synchronized (activeHandlers) {
                    activeHandlers.remove(SocketHandler.this);
                }
            }
            System.err.println("DBG>Input handler for " + clientID + " has finished.");
        }

    }
}

public class Server {

    public static void main(String[] args) {
        int port = 33000, max_conn = 2;

        if (args.length > 0) {
            if (args[0].startsWith("--help")) {
                System.out.printf("Usage: Server [PORT] [MAX_CONNECTIONS]\n" +
                        "If PORT is not specified, default port %d is used\n" +
                        "If MAX_CONNECTIONS is not specified, default number=%d is used", port, max_conn);
                return;
            }
            try {
                port = Integer.decode(args[0]);
            } catch (NumberFormatException e) {
                System.err.printf("Argument %s is not integer, using default value", args[0]); //, port
            }
            if (args.length > 1) try {
                max_conn = Integer.decode(args[1]);
            } catch (NumberFormatException e) {
                System.err.printf("Argument %s is not integer, using default value", args[1]); //, max_conn
            }

        }
        // TODO Auto-generated method stub
        System.out.printf("IM server listening on port %d, maximum nr. of connections=%d...\n", port, max_conn);
        ExecutorService pool = Executors.newFixedThreadPool(2 * max_conn);
        server.ActiveHandlers activeHandlers = new server.ActiveHandlers();

        try {
            ServerSocket sSocket = new ServerSocket(port);
            do {
                Socket clientSocket = sSocket.accept();
                clientSocket.setKeepAlive(true);
                server.SocketHandler handler = new server.SocketHandler(clientSocket, activeHandlers);
                pool.execute(handler.inputHandler);
                pool.execute(handler.outputHandler);
            } while (!pool.isTerminated());
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
            pool.shutdown();
            try {
                // Wait a while for existing tasks to terminate
                if (!pool.awaitTermination(60, TimeUnit.SECONDS)) {
                    pool.shutdownNow(); // Cancel currently executing tasks
                    // Wait a while for tasks to respond to being cancelled
                    if (!pool.awaitTermination(60, TimeUnit.SECONDS))
                        System.err.println("Pool did not terminate");
                }
            } catch (InterruptedException ie) {
                // (Re-)Cancel if current thread also interrupted
                pool.shutdownNow();
                // Preserve interrupt status
                Thread.currentThread().interrupt();
            }
        }
    }
}


